package com.everis.demo.di.component;

import android.app.Application;

import com.everis.MyApplication;
import com.everis.data.di.NetModule;
import com.everis.data.di.RoomModule;
import com.everis.demo.di.module.ActivityModule;
import com.everis.demo.di.module.ContextModule;
import com.everis.demo.di.module.FragmentModule;
import com.everis.demo.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ActivityModule.class,
        FragmentModule.class,
        NetModule.class,
        ContextModule.class,
        RoomModule.class,
        AppModule.class,
})
public interface AppComponent extends AndroidInjector<MyApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }


}
