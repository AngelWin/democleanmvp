package com.everis.demo.di.module;

import com.everis.data.executor.JobExecutor;
import com.everis.data.repository.MoviesDataRepository;
import com.everis.data.repository.datasource.CloudMoviesDataStoreImpl;
import com.everis.data.repository.datasource.LocalMoviesDataStoreImpl;
import com.everis.data.repository.datasource.interfaces.CloudMoviesDataStore;
import com.everis.data.repository.datasource.interfaces.LocalMoviesDataStore;
import com.everis.demo.UIThread;
import com.everis.domain.executor.PostExecutionThread;
import com.everis.domain.executor.ThreadExecutor;
import com.everis.domain.repository.MoviesRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    CloudMoviesDataStore provideCloudMovieDataStore(CloudMoviesDataStoreImpl cloudMoviesDataStore) {
        return cloudMoviesDataStore;
    }

    @Provides
    @Singleton
    LocalMoviesDataStore provideLocalMovieDataStore(LocalMoviesDataStoreImpl localMoviesDataStore) {
        return localMoviesDataStore;
    }

    @Provides
    @Singleton
    MoviesRepository provideMoviesRepository(MoviesDataRepository moviesDataRepository) {
        return moviesDataRepository;
    }

}
