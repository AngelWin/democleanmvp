package com.everis.demo.di.module;

import com.everis.demo.ui.popular_movies.PopularMoviesFragment;
import com.everis.demo.ui.popular_movies.PopularMoviesModule;
import com.everis.demo.ui.top_rated_movies.TopRatedMoviesFragment;
import com.everis.demo.ui.top_rated_movies.TopRatedMoviesModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {

    @ContributesAndroidInjector(modules = PopularMoviesModule.class)
    abstract PopularMoviesFragment contributePopularMoviesFragment();

    @ContributesAndroidInjector(modules = TopRatedMoviesModule.class)
    abstract TopRatedMoviesFragment contributeTopRatedMoviesFragment();
}
