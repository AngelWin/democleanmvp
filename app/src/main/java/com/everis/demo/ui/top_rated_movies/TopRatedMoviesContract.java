package com.everis.demo.ui.top_rated_movies;

import com.everis.demo.model.MovieModel;
import com.everis.demo.ui.base.BaseContract;

import java.util.List;

public interface TopRatedMoviesContract {

    interface Presenter extends BaseContract.Presenter {

        void init();

        void getTopRatedMovies();
    }

    interface View extends BaseContract.View {

        void onSuccessMovies(List<MovieModel> movies);

        void onErrorMovies();
    }
}
