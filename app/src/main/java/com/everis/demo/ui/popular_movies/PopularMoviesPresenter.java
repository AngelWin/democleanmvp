package com.everis.demo.ui.popular_movies;

import com.everis.demo.model.mapper.MovieModelDataMapper;
import com.everis.demo.ui.base.BasePresenter;
import com.everis.domain.bean.Movie;
import com.everis.domain.interactor.GetPopularMovies;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

public class PopularMoviesPresenter extends
        BasePresenter<PopularMoviesContract.View>
        implements PopularMoviesContract.Presenter {

    private final GetPopularMovies getPopularMoviesUseCase;
    private final MovieModelDataMapper movieModelDataMapper;

    @Inject
    public PopularMoviesPresenter(PopularMoviesContract.View view,
                                  GetPopularMovies getPopularMoviesUseCase,
                                  MovieModelDataMapper movieModelDataMapper) {
        super(view);
        this.getPopularMoviesUseCase = getPopularMoviesUseCase;
        this.movieModelDataMapper = movieModelDataMapper;
    }

    @Override
    public void init() {
        getPopularMovies();
    }

    @Override
    public void getPopularMovies() {
        getPopularMoviesUseCase.execute(new DisposableSingleObserver<List<Movie>>() {
            @Override
            public void onSuccess(List<Movie> movies) {
                view.hideLoading();
                view.onSuccessMovies(movieModelDataMapper.transform(movies));
            }

            @Override
            public void onError(Throwable e) {
                view.hideLoading();
                view.onErrorMovies();
            }
        }, new GetPopularMovies.Params(1));
    }

    @Override
    public void destroy() {
        getPopularMoviesUseCase.clear();
    }

    @Override
    public void stop() {

    }

    @Override
    public void resume() {

    }
}
