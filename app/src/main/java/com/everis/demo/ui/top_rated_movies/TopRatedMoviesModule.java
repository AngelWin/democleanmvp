package com.everis.demo.ui.top_rated_movies;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class TopRatedMoviesModule {

    @Binds
    abstract TopRatedMoviesContract.Presenter providePopularMoviesPresenter(TopRatedMoviesPresenter presenter);

    @Binds
    abstract TopRatedMoviesContract.View providePopularMoviesView(TopRatedMoviesFragment topRatedMoviesFragment);
}
