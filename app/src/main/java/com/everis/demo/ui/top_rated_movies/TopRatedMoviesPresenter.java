package com.everis.demo.ui.top_rated_movies;

import com.everis.demo.model.mapper.MovieModelDataMapper;
import com.everis.demo.ui.base.BasePresenter;
import com.everis.domain.bean.Movie;
import com.everis.domain.interactor.GetTopRatedMovies;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

public class TopRatedMoviesPresenter extends
        BasePresenter<TopRatedMoviesContract.View>
        implements TopRatedMoviesContract.Presenter {

    private final GetTopRatedMovies getTopRatedMoviesUseCase;
    private final MovieModelDataMapper movieModelDataMapper;

    @Inject
    public TopRatedMoviesPresenter(TopRatedMoviesContract.View view,
                                   GetTopRatedMovies getTopRatedMoviesUseCase,
                                   MovieModelDataMapper movieModelDataMapper) {
        super(view);
        this.getTopRatedMoviesUseCase = getTopRatedMoviesUseCase;
        this.movieModelDataMapper = movieModelDataMapper;
    }

    @Override
    public void init() {
        getTopRatedMovies();
    }

    @Override
    public void getTopRatedMovies() {
        getTopRatedMoviesUseCase.execute(new DisposableSingleObserver<List<Movie>>() {
            @Override
            public void onSuccess(List<Movie> movies) {
                view.hideLoading();
                view.onSuccessMovies(movieModelDataMapper.transform(movies));
            }

            @Override
            public void onError(Throwable e) {
                view.hideLoading();
                view.onErrorMovies();
            }
        }, new GetTopRatedMovies.Params(1));
    }

    @Override
    public void destroy() {
        getTopRatedMoviesUseCase.dispose();
    }

    @Override
    public void stop() {

    }

    @Override
    public void resume() {

    }
}
