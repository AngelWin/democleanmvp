package com.everis.demo.ui.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.everis.demo.R;
import com.everis.demo.ui.base.BaseActivity;
import com.everis.demo.ui.popular_movies.PopularMoviesFragment;
import com.everis.demo.ui.top_rated_movies.TopRatedMoviesFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.frContent)
    FrameLayout frContent;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private FragmentManager fragmentManager;


    private PopularMoviesFragment popularMoviesFragment;
    private TopRatedMoviesFragment topRatedMoviesFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
    }


    private void init() {
        fragmentManager = getSupportFragmentManager();
        navigation.setOnNavigationItemSelectedListener(this);
        popularMoviesFragment = PopularMoviesFragment.newInstance();
        topRatedMoviesFragment = TopRatedMoviesFragment.newInstance();
        navigation.setSelectedItemId(R.id.action_popular);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_popular:
                showPopularMovies();
                return true;
            case R.id.action_top_rated:
                showTopRatedMovies();
                return true;
            case R.id.action_settings:
                showSettings();
                return true;
            default:
                return false;
        }

    }

    private void showPopularMovies() {
        tvTitle.setText(getString(R.string.popular_movies));
        if (popularMoviesFragment != null && popularMoviesFragment.isAdded()) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (topRatedMoviesFragment != null) {
                ft.hide(topRatedMoviesFragment);
            }
            ft.show(popularMoviesFragment);
            ft.commit();
        } else {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.add(R.id.frContent, popularMoviesFragment);
            ft.commitAllowingStateLoss();
        }
    }

    private void showTopRatedMovies() {
        tvTitle.setText(getString(R.string.top_rated_movies));
        if (topRatedMoviesFragment != null && topRatedMoviesFragment.isAdded()) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (popularMoviesFragment != null) {
                ft.hide(popularMoviesFragment);
            }
            ft.show(topRatedMoviesFragment);
            ft.commit();
        } else {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.add(R.id.frContent, topRatedMoviesFragment);
            ft.commitAllowingStateLoss();
        }
    }

    private void showSettings() {

    }

}
