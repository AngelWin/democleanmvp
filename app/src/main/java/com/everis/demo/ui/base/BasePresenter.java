package com.everis.demo.ui.base;

public abstract class BasePresenter<T> {

    protected T view;

    protected BasePresenter(T view) {
        this.view = view;
    }

}
