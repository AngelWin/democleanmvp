package com.everis.demo.ui.top_rated_movies;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.everis.demo.R;
import com.everis.demo.model.MovieModel;
import com.everis.demo.ui.adapter.MoviesAdapter;
import com.everis.demo.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TopRatedMoviesFragment extends BaseFragment implements TopRatedMoviesContract.View {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.rvMovies)
    RecyclerView rvMovies;
    Unbinder unbinder;

    @Inject
    TopRatedMoviesContract.Presenter presenter;

    private MoviesAdapter moviesAdapter;

    public static TopRatedMoviesFragment newInstance() {
        return new TopRatedMoviesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_rated, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void init() {
        configureRecycler();
        presenter.init();
    }

    private void configureRecycler() {
        rvMovies.setLayoutManager(new GridLayoutManager(getContext(), 2));
        moviesAdapter = new MoviesAdapter();
        rvMovies.setAdapter(moviesAdapter);
    }

    @Override
    public void onSuccessMovies(List<MovieModel> movies) {
        rvMovies.setVisibility(View.VISIBLE);
        moviesAdapter.updateMovies(movies);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @Override
    public void onErrorMovies() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        progress.setVisibility(View.GONE);
    }
}
