package com.everis.demo.ui.popular_movies;

import com.everis.demo.model.MovieModel;
import com.everis.demo.ui.base.BaseContract;

import java.util.List;

public interface PopularMoviesContract {

    interface Presenter extends BaseContract.Presenter {

        void init();

        void getPopularMovies();
    }

    interface View extends BaseContract.View {

        void onSuccessMovies(List<MovieModel> movies);

        void onErrorMovies();
    }
}
