package com.everis.demo.ui.popular_movies;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class PopularMoviesModule {

    @Binds
    abstract PopularMoviesContract.Presenter providePopularMoviesPresenter(PopularMoviesPresenter presenter);

    @Binds
    abstract PopularMoviesContract.View providePopularMoviesView(PopularMoviesFragment popularMoviesFragment);


}
