package com.everis.demo.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.everis.demo.R;
import com.everis.demo.model.MovieModel;
import com.everis.demo.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {

    private List<MovieModel> movies = new ArrayList<>();

    public void updateMovies(List<MovieModel> movies) {
        if (movies != null) {
            this.movies.clear();
            this.movies.addAll(movies);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_movie, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        MovieModel movie = movies.get(i);
        viewHolder.bind(movie);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivMovie;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivMovie = itemView.findViewById(R.id.ivMovie);
        }

        private void bind(MovieModel movie) {
            Glide.with(itemView.getContext())
                    .load(Constants.BASE_IMAGE_URL + movie.getPoster_path())
                    .into(ivMovie);
        }
    }
}
