package com.everis.demo.ui.base;

public interface BaseContract {

    interface Presenter {
        void destroy();

        void resume();

        void stop();
    }

    interface View {
        void showLoading();

        void hideLoading();

        void showError(String message);
    }
}
