package com.everis.demo.model.mapper;

import com.everis.demo.model.MovieModel;
import com.everis.domain.bean.Movie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

public class MovieModelDataMapper {

    @Inject
    public MovieModelDataMapper() {
    }

    public MovieModel transform(Movie businessObject) {
        if (businessObject == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        final MovieModel model = new MovieModel();
        model.setId(businessObject.getId());
        model.setAdult(businessObject.getAdult());
        model.setBackdrop_path(businessObject.getBackdrop_path());
        model.setOriginal_language(businessObject.getOriginal_language());
        model.setOverview(businessObject.getOverview());
        model.setPopularity(businessObject.getPopularity());
        model.setPoster_path(businessObject.getPoster_path());
        model.setRelease_date(businessObject.getRelease_date());
        model.setTitle(businessObject.getTitle());
        model.setVideo(businessObject.isVideo());
        model.setVote_average(businessObject.getVote_average());
        model.setVote_count(businessObject.getVote_count());
        return model;
    }

    public List<MovieModel> transform(List<Movie> businessCollection) {
        List<MovieModel> modelCollection;
        if (businessCollection != null && !businessCollection.isEmpty()) {
            modelCollection = new ArrayList<>();
            for (Movie businessObject : businessCollection) {
                modelCollection.add(transform(businessObject));
            }
        } else {
            modelCollection = Collections.emptyList();
        }
        return modelCollection;
    }
}
