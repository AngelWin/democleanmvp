package com.everis.demo.util;

public interface Constants {

    String BASE_IMAGE_URL = "http://image.tmdb.org/t/p/w500";
    String DEFAULT_LANGUAGE = "en-US";
}
