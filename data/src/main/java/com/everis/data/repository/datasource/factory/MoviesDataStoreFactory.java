package com.everis.data.repository.datasource.factory;

import com.everis.data.repository.datasource.interfaces.CloudMoviesDataStore;
import com.everis.data.repository.datasource.interfaces.LocalMoviesDataStore;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MoviesDataStoreFactory {

    private final CloudMoviesDataStore cloudMoviesDataStore;
    private final LocalMoviesDataStore localMoviesDataStore;

    @Inject
    public MoviesDataStoreFactory(CloudMoviesDataStore cloudMoviesDataStore,
                                  LocalMoviesDataStore localMoviesDataStore) {
        this.cloudMoviesDataStore = cloudMoviesDataStore;
        this.localMoviesDataStore = localMoviesDataStore;
    }

    public CloudMoviesDataStore createCloud() {
        CloudMoviesDataStore dataStore;
        dataStore = createCloudDataStore();
        return dataStore;
    }

    public LocalMoviesDataStore createLocal() {
        LocalMoviesDataStore dataStore;
        dataStore = createLocalDataStore();
        return dataStore;
    }

    private CloudMoviesDataStore createCloudDataStore() {
        return cloudMoviesDataStore;
    }

    private LocalMoviesDataStore createLocalDataStore() {
        return localMoviesDataStore;
    }


}
