package com.everis.data.repository;

import com.everis.data.repository.datasource.factory.PreferencesDataStoreFactory;
import com.everis.data.repository.datasource.interfaces.PreferencesDataStore;
import com.everis.domain.repository.PreferencesRepository;

import javax.inject.Inject;

public class PreferencesDataRepository implements PreferencesRepository {

    private final PreferencesDataStoreFactory preferencesDataStoreFactory;

    @Inject
    public PreferencesDataRepository(PreferencesDataStoreFactory preferencesDataStoreFactory) {
        this.preferencesDataStoreFactory = preferencesDataStoreFactory;
    }

    @Override
    public void setToken(String token) {
        final PreferencesDataStore dataStore = preferencesDataStoreFactory.create();
        dataStore.setToken(token);
    }

    @Override
    public String getToken() {
        final PreferencesDataStore dataStore = preferencesDataStoreFactory.create();
        return dataStore.getToken();
    }
}
