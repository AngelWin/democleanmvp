package com.everis.data.repository.datasource;

import android.content.Context;
import android.content.SharedPreferences;

import com.everis.data.repository.datasource.interfaces.PreferencesDataStore;

import javax.inject.Inject;

public class PreferencesDataStoreImpl implements PreferencesDataStore {


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private static final String SHARED_PREF_NAME = "DEMO_PREFERENCES";
    private static final String KEY_TOKEN = "TOKEN";

    @Inject
    PreferencesDataStoreImpl(Context context) {
        this.sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void setToken(String token) {
        editor = sharedPreferences.edit();
        editor.putString(KEY_TOKEN, token);
        editor.apply();
    }

    @Override
    public String getToken() {
        return sharedPreferences.getString(KEY_TOKEN, null);
    }
}
