package com.everis.data.repository.datasource.factory;

import com.everis.data.repository.datasource.interfaces.PreferencesDataStore;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesDataStoreFactory {

    private final PreferencesDataStore preferencesDataStore;

    @Inject
    public PreferencesDataStoreFactory(PreferencesDataStore preferencesDataStore) {
        this.preferencesDataStore = preferencesDataStore;
    }

    public PreferencesDataStore create() {
        PreferencesDataStore dataStore;
        dataStore = createDataStore();
        return dataStore;
    }

    private PreferencesDataStore createDataStore() {
        return preferencesDataStore;
    }


}
