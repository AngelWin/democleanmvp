package com.everis.data.repository.datasource.interfaces;

public interface PreferencesDataStore {

    void setToken(String token);

    String getToken();
}
