package com.everis.data.repository.datasource;

import com.everis.data.bean.MovieDto;
import com.everis.data.bean.MoviesDto;
import com.everis.data.net.ApiConstants;
import com.everis.data.net.RestApi;
import com.everis.data.repository.datasource.interfaces.CloudMoviesDataStore;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class CloudMoviesDataStoreImpl implements CloudMoviesDataStore {

    private final RestApi restApi;

    @Inject
    CloudMoviesDataStoreImpl(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Single<List<MovieDto>> fetchPopularMovies(int page) {
        return restApi.fetchPopularMovies(ApiConstants.API_MOVIE_KEY, "en", page, "")
                .map(MoviesDto::getResults);
    }

    @Override
    public Single<List<MovieDto>> fetchTopRatedMovies(int page) {
        return restApi.fetchTopRatedMovies(ApiConstants.API_MOVIE_KEY, "en", page, "")
                .map(MoviesDto::getResults);
    }
}
