package com.everis.data.repository.datasource;

import com.everis.data.bean.MovieCartDto;
import com.everis.data.database.dao.MovieCartDao;
import com.everis.data.repository.datasource.interfaces.LocalMoviesDataStore;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class LocalMoviesDataStoreImpl implements LocalMoviesDataStore {

    private final MovieCartDao movieCartDao;

    @Inject
    public LocalMoviesDataStoreImpl(MovieCartDao movieCartDao) {
        this.movieCartDao = movieCartDao;
    }

    @Override
    public void insert(MovieCartDto movie) {
        movieCartDao.insert(movie);
    }

    @Override
    public Single<List<MovieCartDto>> getAll() {
        return movieCartDao.getAllMovies();
    }

    @Override
    public void deleteAll() {
        movieCartDao.deleteAll();
    }
}
