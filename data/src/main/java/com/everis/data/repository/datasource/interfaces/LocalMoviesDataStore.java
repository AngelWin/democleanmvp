package com.everis.data.repository.datasource.interfaces;

import com.everis.data.bean.MovieCartDto;

import java.util.List;

import io.reactivex.Single;

public interface LocalMoviesDataStore {

    void insert(MovieCartDto movie);

    Single<List<MovieCartDto>> getAll();

    void deleteAll();
}
