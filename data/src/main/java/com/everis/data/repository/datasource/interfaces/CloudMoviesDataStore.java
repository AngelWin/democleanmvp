package com.everis.data.repository.datasource.interfaces;

import com.everis.data.bean.MovieDto;

import java.util.List;

import io.reactivex.Single;

public interface CloudMoviesDataStore {

    Single<List<MovieDto>> fetchPopularMovies(int page);

    Single<List<MovieDto>> fetchTopRatedMovies(int page);

}
