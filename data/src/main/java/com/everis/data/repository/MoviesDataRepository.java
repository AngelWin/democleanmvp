package com.everis.data.repository;

import android.content.Context;

import com.everis.data.bean.mapper.MovieCartDtoDataMapper;
import com.everis.data.bean.mapper.MovieDtoDataMapper;
import com.everis.data.repository.datasource.factory.MoviesDataStoreFactory;
import com.everis.data.repository.datasource.interfaces.CloudMoviesDataStore;
import com.everis.data.repository.datasource.interfaces.LocalMoviesDataStore;
import com.everis.domain.bean.Movie;
import com.everis.domain.bean.MovieCart;
import com.everis.domain.repository.MoviesRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class MoviesDataRepository implements MoviesRepository {

    private final MoviesDataStoreFactory moviesDataStoreFactory;
    private final MovieDtoDataMapper movieDtoDataMapper;
    private final MovieCartDtoDataMapper movieCartDtoDataMapper;

    @Inject
    public MoviesDataRepository(MoviesDataStoreFactory moviesDataStoreFactory,
                                MovieDtoDataMapper movieDtoDataMapper,
                                MovieCartDtoDataMapper movieCartDtoDataMapper) {
        this.moviesDataStoreFactory = moviesDataStoreFactory;
        this.movieDtoDataMapper = movieDtoDataMapper;
        this.movieCartDtoDataMapper = movieCartDtoDataMapper;
    }

    @Override
    public Single<List<Movie>> getPopularMovies(int page) {
        final CloudMoviesDataStore cloudMoviesDataStore = moviesDataStoreFactory.createCloud();
        return cloudMoviesDataStore.fetchPopularMovies(page).map(movieDtoDataMapper::transform);
    }

    @Override
    public Single<List<Movie>> getTopRatedMovies(int page) {
        final CloudMoviesDataStore cloudMoviesDataStore = moviesDataStoreFactory.createCloud();
        return cloudMoviesDataStore.fetchTopRatedMovies(page).map(movieDtoDataMapper::transform);
    }

    @Override
    public Completable insert(MovieCart movie) {
        final LocalMoviesDataStore localMoviesDataStore = moviesDataStoreFactory.createLocal();
        return Completable.fromAction(() ->
                localMoviesDataStore.insert(movieCartDtoDataMapper.transform(movie)));
    }

    @Override
    public Single<List<MovieCart>> getAllMoviesCart() {
        final LocalMoviesDataStore localMoviesDataStore = moviesDataStoreFactory.createLocal();
        return localMoviesDataStore.getAll().map(movieCartDtoDataMapper::transform);
    }

    @Override
    public Completable deleteAllMovies() {
        final LocalMoviesDataStore localMoviesDataStore = moviesDataStoreFactory.createLocal();
        return Completable.fromAction(localMoviesDataStore::deleteAll);
    }
}
