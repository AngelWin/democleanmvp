package com.everis.data.di;

import android.content.Context;

import com.everis.data.database.MovieDataBase;
import com.everis.data.database.dao.MovieCartDao;
import com.everis.data.repository.datasource.LocalMoviesDataStoreImpl;
import com.everis.data.repository.datasource.interfaces.LocalMoviesDataStore;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class RoomModule {

    @Singleton
    @Provides
    MovieDataBase providesDatabase(Context context) {
        return MovieDataBase.getDatabase(context);
    }

    @Singleton
    @Provides
    MovieCartDao providesMovieCartDao(MovieDataBase dataBase) {
        return dataBase.movieCartDao();
    }

}
