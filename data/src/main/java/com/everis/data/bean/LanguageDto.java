package com.everis.data.bean;

public class LanguageDto {

    private String iso_639_1;
    private String name;

    public String getIso_639_1() {
        return iso_639_1;
    }

    public String getName() {
        return name;
    }
}
