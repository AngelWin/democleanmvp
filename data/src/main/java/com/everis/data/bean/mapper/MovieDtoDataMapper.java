package com.everis.data.bean.mapper;

import com.everis.data.bean.MovieDto;
import com.everis.domain.bean.Movie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MovieDtoDataMapper {

    @Inject
    public MovieDtoDataMapper() {
    }

    public Movie transform(MovieDto dto) {
        if (dto == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        final Movie businessObject = new Movie();
        businessObject.setId(dto.getId());
        businessObject.setAdult(dto.getAdult());
        businessObject.setBackdrop_path(dto.getBackdrop_path());
        businessObject.setOriginal_language(dto.getOriginal_language());
        businessObject.setOverview(dto.getOverview());
        businessObject.setPopularity(dto.getPopularity());
        businessObject.setPoster_path(dto.getPoster_path());
        businessObject.setRelease_date(dto.getRelease_date());
        businessObject.setTitle(dto.getTitle());
        businessObject.setVideo(dto.isVideo());
        businessObject.setVote_average(dto.getVote_average());
        businessObject.setVote_count(dto.getVote_count());
        return businessObject;
    }

    public List<Movie> transform(List<MovieDto> dtoCollection) {
        List<Movie> businessCollection;
        if (dtoCollection != null && !dtoCollection.isEmpty()) {
            businessCollection = new ArrayList<>();
            for (MovieDto dto : dtoCollection) {
                businessCollection.add(transform(dto));
            }
        } else {
            businessCollection = Collections.emptyList();
        }
        return businessCollection;
    }
}
