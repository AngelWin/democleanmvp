package com.everis.data.bean;

import java.util.List;

public class MoviesDto {

    private String page;
    private int total_results;
    private int total_pages;
    private List<MovieDto> results;

    public String getPage() {
        return page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public List<MovieDto> getResults() {
        return results;
    }
}
