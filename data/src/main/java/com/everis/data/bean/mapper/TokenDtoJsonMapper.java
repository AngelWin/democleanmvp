package com.everis.data.bean.mapper;

import com.everis.data.bean.TokenDto;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TokenDtoJsonMapper {

    private final Gson gson;

    @Inject
    public TokenDtoJsonMapper() {
        this.gson = new Gson();
    }

    public TokenDto transform(String response) throws JsonSyntaxException {
        final Type type = new TypeToken<TokenDto>() {
        }.getType();
        return this.gson.fromJson(response, type);
    }

}