package com.everis.data.bean;

public class ProductionCompanyDto {

    private int id;
    private String logo_path;
    private String name;
    private String origin_country;

    public int getId() {
        return id;
    }

    public String getLogo_path() {
        return logo_path;
    }

    public String getName() {
        return name;
    }

    public String getOrigin_country() {
        return origin_country;
    }
}
