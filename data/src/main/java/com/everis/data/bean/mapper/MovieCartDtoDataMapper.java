package com.everis.data.bean.mapper;

import com.everis.data.bean.MovieCartDto;
import com.everis.domain.bean.MovieCart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MovieCartDtoDataMapper {

    @Inject
    public MovieCartDtoDataMapper() {
    }

    public MovieCart transform(MovieCartDto dto) {
        if (dto == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        final MovieCart businessObject = new MovieCart();
        businessObject.setMovieId(dto.getMovieId());
        businessObject.setMoviePosterPath(dto.getMoviePosterPath());
        businessObject.setMovieTitle(dto.getMovieTitle());
        businessObject.setQuantity(dto.getQuantity());
        return businessObject;
    }

    public MovieCartDto transform(MovieCart businessObject) {
        if (businessObject == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        final MovieCartDto dto = new MovieCartDto();
        dto.setMovieId(businessObject.getMovieId());
        dto.setMoviePosterPath(businessObject.getMoviePosterPath());
        dto.setMovieTitle(businessObject.getMovieTitle());
        dto.setQuantity(businessObject.getQuantity());
        return dto;
    }

    public List<MovieCart> transform(List<MovieCartDto> dtoCollection) {
        List<MovieCart> businessCollection;
        if (dtoCollection != null && !dtoCollection.isEmpty()) {
            businessCollection = new ArrayList<>();
            for (MovieCartDto dto : dtoCollection) {
                businessCollection.add(transform(dto));
            }
        } else {
            businessCollection = Collections.emptyList();
        }
        return businessCollection;
    }
}
