package com.everis.data.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.everis.data.bean.MovieCartDto;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface MovieCartDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MovieCartDto movie);

    @Query("SELECT * FROM MovieCartDto")
    Single<List<MovieCartDto>> getAllMovies();

    @Query("DELETE FROM MovieCartDto")
    void deleteAll();

}
