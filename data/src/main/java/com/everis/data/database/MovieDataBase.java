package com.everis.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.everis.data.bean.MovieCartDto;
import com.everis.data.database.dao.MovieCartDao;

@Database(entities = {MovieCartDto.class},
        version = MovieDataBase.VERSION, exportSchema = false)
public abstract class MovieDataBase extends RoomDatabase {

    static final int VERSION = 1;

    private static final String DB_NAME = "MovieDB";
    private static MovieDataBase INSTANCE;

    public abstract MovieCartDao movieCartDao();

    public static MovieDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MovieDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MovieDataBase.class, DB_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
