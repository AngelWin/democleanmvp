package com.everis.data.net.interceptor;

import android.support.annotation.NonNull;

import com.everis.data.BuildConfig;
import com.everis.data.bean.mapper.TokenDtoJsonMapper;
import com.everis.data.di.NetModule;
import com.everis.data.net.ApiConstants;
import com.everis.data.repository.datasource.interfaces.PreferencesDataStore;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AuthTokenInterceptor implements Interceptor {

    private final OkHttpClient okHttpClient;
    private final TokenDtoJsonMapper tokenDtoJsonMapper;
    private final PreferencesDataStore preferencesDataStore;

    @Inject
    public AuthTokenInterceptor(@Named(NetModule.OK_HTTP) OkHttpClient okHttpClient,
                                TokenDtoJsonMapper tokenDtoJsonMapper,
                                PreferencesDataStore preferencesDataStore) {
        this.okHttpClient = okHttpClient;
        this.tokenDtoJsonMapper = tokenDtoJsonMapper;
        this.preferencesDataStore = preferencesDataStore;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request mainRequest = chain.request();
        String token = preferencesDataStore.getToken();
        if (token != null) {
            Request.Builder builder = mainRequest.newBuilder()
                    .header("access_token", token)
                    .method(mainRequest.method(), mainRequest.body());
            Response response = chain.proceed(builder.build());
            if (response.code() == 401) {
                String newToken = getToken(okHttpClient);
                Request.Builder newBuilder = mainRequest.newBuilder()
                        .header("access_token", newToken)
                        .method(mainRequest.method(), mainRequest.body());
                return chain.proceed(newBuilder.build());
            } else {
                return response;
            }
        } else {
            String newToken = getToken(okHttpClient);
            Request.Builder newBuilder = mainRequest.newBuilder()
                    .header("access_token", newToken)
                    .method(mainRequest.method(), mainRequest.body());
            return chain.proceed(newBuilder.build());
        }
    }

    private String getToken(OkHttpClient okHttpClient) throws IOException {
        HashMap<String, String> tokenRequest = new HashMap<>();
        tokenRequest.put("client_id", BuildConfig.CLIENT_ID);
        tokenRequest.put("client_secret", BuildConfig.CLIENT_SECRET);
        String json = new JSONObject(tokenRequest).toString();
        RequestBody parameter = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=UTF-8"), json);
        final Request request = new Request.Builder()
                .url(ApiConstants.END_POINT + ApiConstants.PATH_TOKEN)
                .post(parameter)
                .build();
        try {
            String tokenEntityResponse = okHttpClient.newCall(request).execute().body().string();
            return tokenDtoJsonMapper.transform(tokenEntityResponse).getAccess_token();
        } catch (Exception ex) {
            throw new IOException(ex.getMessage());
        }
    }
}
