package com.everis.data.net;


import com.everis.data.bean.MoviesDto;

import io.reactivex.Single;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RestApi {

    @POST(ApiConstants.PATH_POPULAR_MOVIES)
    Single<MoviesDto> fetchPopularMovies(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page,
            @Query("region") String region);

    @POST(ApiConstants.PATH_TOP_RATED_MOVIES)
    Single<MoviesDto> fetchTopRatedMovies(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page,
            @Query("region") String region);

}


