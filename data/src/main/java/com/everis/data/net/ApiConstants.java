package com.everis.data.net;

import com.everis.data.BuildConfig;

public interface ApiConstants {

    String PROTOCOL_HTTPS = "https://";
    String END_POINT = PROTOCOL_HTTPS + BuildConfig.BASE_URL;

    String API_MOVIE_KEY = "954a553c5ea18a7f018c40ac2b781b2d";

    String PATH_POPULAR_MOVIES = "movie/popular";
    String PATH_TOP_RATED_MOVIES = "movie/top_rated";
    String PATH_TOKEN = "api/generatoken";
}
