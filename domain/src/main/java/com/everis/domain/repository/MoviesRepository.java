package com.everis.domain.repository;

import com.everis.domain.bean.Movie;
import com.everis.domain.bean.MovieCart;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface MoviesRepository {

    Single<List<Movie>> getPopularMovies(int page);

    Single<List<Movie>> getTopRatedMovies(int page);

    Completable insert(MovieCart movie);

    Single<List<MovieCart>> getAllMoviesCart();

    Completable deleteAllMovies();
}
