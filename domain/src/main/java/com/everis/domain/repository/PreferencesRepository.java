package com.everis.domain.repository;

public interface PreferencesRepository {

    void setToken(String token);

    String getToken();
}
