package com.everis.domain.interactor;

import com.everis.domain.bean.MovieCart;
import com.everis.domain.executor.PostExecutionThread;
import com.everis.domain.executor.ThreadExecutor;
import com.everis.domain.interactor.base.CompletableUseCase;
import com.everis.domain.repository.MoviesRepository;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import io.reactivex.Completable;

public class AddMovieCart extends CompletableUseCase<AddMovieCart.Params> {

    private final MoviesRepository moviesRepository;

    @Inject
    AddMovieCart(ThreadExecutor threadExecutor,
                 PostExecutionThread postExecutionThread,
                 MoviesRepository moviesRepository) {
        super(threadExecutor, postExecutionThread);
        this.moviesRepository = moviesRepository;
    }

    @Override
    public Completable buildUseCaseCompletable(Params params) {
        Preconditions.checkNotNull(params.movieCart);
        return moviesRepository.insert(params.movieCart);
    }

    public static final class Params {

        private final MovieCart movieCart;

        public Params(MovieCart movieCart) {
            this.movieCart = movieCart;
        }
    }

}
