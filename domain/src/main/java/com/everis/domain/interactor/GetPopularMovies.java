package com.everis.domain.interactor;

import com.everis.domain.bean.Movie;
import com.everis.domain.executor.PostExecutionThread;
import com.everis.domain.executor.ThreadExecutor;
import com.everis.domain.interactor.base.SingleUseCase;
import com.everis.domain.repository.MoviesRepository;

import java.util.List;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import io.reactivex.Single;

public class GetPopularMovies extends SingleUseCase<List<Movie>, GetPopularMovies.Params> {

    private final MoviesRepository moviesRepository;

    @Inject
    GetPopularMovies(ThreadExecutor threadExecutor,
                     PostExecutionThread postExecutionThread,
                     MoviesRepository moviesRepository) {
        super(threadExecutor, postExecutionThread);
        this.moviesRepository = moviesRepository;
    }

    @Override
    public Single<List<Movie>> buildUseCaseObservable(GetPopularMovies.Params params) {
        Preconditions.checkNotNull(params.page);
        return moviesRepository.getPopularMovies(params.page);
    }

    public static final class Params {

        private final int page;

        public Params(int page) {
            this.page = page;
        }
    }


}
