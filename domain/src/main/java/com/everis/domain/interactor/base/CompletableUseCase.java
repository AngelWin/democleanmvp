package com.everis.domain.interactor.base;


import com.everis.domain.executor.PostExecutionThread;
import com.everis.domain.executor.ThreadExecutor;

import dagger.internal.Preconditions;
import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;


public abstract class CompletableUseCase<Params> {

    protected final ThreadExecutor threadExecutor;
    protected final PostExecutionThread postExecutionThread;
    protected CompositeDisposable disposables;

    public CompletableUseCase(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
        this.disposables = new CompositeDisposable();
    }

    public abstract Completable buildUseCaseCompletable(Params params);


    public void execute(DisposableCompletableObserver completableObserver, Params params) {
        Preconditions.checkNotNull(completableObserver);
        final Completable completable = this.buildUseCaseCompletable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
        addDisposable(completable.subscribeWith(completableObserver));
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    public void clear() {
        if (!disposables.isDisposed()) {
            disposables.clear();
        }
    }

    protected void addDisposable(Disposable disposable) {
        Preconditions.checkNotNull(disposable);
        Preconditions.checkNotNull(disposables);
        disposables.add(disposable);
    }
}