package com.everis.domain.interactor;

import com.everis.domain.bean.MovieCart;
import com.everis.domain.executor.PostExecutionThread;
import com.everis.domain.executor.ThreadExecutor;
import com.everis.domain.interactor.base.SingleUseCase;
import com.everis.domain.repository.MoviesRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetLocalMoviesCart extends SingleUseCase<List<MovieCart>, Void> {

    private final MoviesRepository moviesRepository;

    @Inject
    GetLocalMoviesCart(ThreadExecutor threadExecutor,
                       PostExecutionThread postExecutionThread,
                       MoviesRepository moviesRepository) {
        super(threadExecutor, postExecutionThread);
        this.moviesRepository = moviesRepository;
    }

    @Override
    public Single<List<MovieCart>> buildUseCaseObservable(Void aVoid) {
        return moviesRepository.getAllMoviesCart();
    }
}
