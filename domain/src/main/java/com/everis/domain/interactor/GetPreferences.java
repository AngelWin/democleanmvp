package com.everis.domain.interactor;

import com.everis.domain.repository.PreferencesRepository;

import javax.inject.Inject;

public class GetPreferences {

    private final PreferencesRepository preferencesRepository;

    @Inject
    public GetPreferences(PreferencesRepository preferencesRepository) {
        this.preferencesRepository = preferencesRepository;
    }

    public void setToken(String token) {
        preferencesRepository.setToken(token);
    }

    public String getToken() {
        return preferencesRepository.getToken();
    }
}
