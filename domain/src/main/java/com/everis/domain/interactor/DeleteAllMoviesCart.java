package com.everis.domain.interactor;

import com.everis.domain.executor.PostExecutionThread;
import com.everis.domain.executor.ThreadExecutor;
import com.everis.domain.interactor.base.CompletableUseCase;
import com.everis.domain.repository.MoviesRepository;

import javax.inject.Inject;

import io.reactivex.Completable;

public class DeleteAllMoviesCart extends CompletableUseCase<Void> {

    private final MoviesRepository moviesRepository;

    @Inject
    DeleteAllMoviesCart(ThreadExecutor threadExecutor,
                        PostExecutionThread postExecutionThread,
                        MoviesRepository moviesRepository) {
        super(threadExecutor, postExecutionThread);
        this.moviesRepository = moviesRepository;
    }

    @Override
    public Completable buildUseCaseCompletable(Void aVoid) {
        return moviesRepository.deleteAllMovies();
    }

}
