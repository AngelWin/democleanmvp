package com.everis.domain.interactor.base;


import com.everis.domain.executor.PostExecutionThread;
import com.everis.domain.executor.ThreadExecutor;

import dagger.internal.Preconditions;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public abstract class SingleUseCase<T, Params> {

    protected final ThreadExecutor threadExecutor;
    protected final PostExecutionThread postExecutionThread;
    protected CompositeDisposable disposables;

    public SingleUseCase(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
        this.disposables = new CompositeDisposable();
    }

    public abstract Single<T> buildUseCaseObservable(Params params);

    public void execute(DisposableSingleObserver<T> singleObserver, Params params) {
        Preconditions.checkNotNull(singleObserver);
        final Single<T> single = this.buildUseCaseObservable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
        addDisposable(single.subscribeWith(singleObserver));
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    public void clear() {
        if (!disposables.isDisposed()) {
            disposables.clear();
        }
    }

    protected void addDisposable(Disposable disposable) {
        Preconditions.checkNotNull(disposable);
        Preconditions.checkNotNull(disposables);
        disposables.add(disposable);
    }
}